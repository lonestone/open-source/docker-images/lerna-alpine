FROM node:11-alpine

WORKDIR /usr/src/app

RUN apk update && apk upgrade && apk add --no-cache bash git openssh
RUN npm install -g lerna
RUN lerna bootstrap

CMD [ "node" ]